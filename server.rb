require "sinatra"
require_relative "./database"

before do
  content_type 'application/json'
  headers 'Access-Control-Allow-Origin' => '*'
end

get "/" do
  current_mood = Mood::Database.database[:moods].order(:id).last
  current_mood.to_json
end
